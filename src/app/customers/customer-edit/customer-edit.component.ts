import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';

import * as customerActions from '../state/customer.actions';
import * as fromCustomer from '../state/customer.reducer';
import { Customer } from '../customer.model';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
  customerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromCustomer.AppState>
  ) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      name: ['', Validators.compose(
        [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(/[a-zA-Z]/)]
      )],
      surname: ['', Validators.compose(
        [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(/[a-zA-Z]/)]
      )],
      gender: ['', Validators.required],
      personalPhoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(9)])],
      country: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      id: null
    });

    const customer$: Observable<Customer> = this.store.select(
      fromCustomer.getCurrentCustomer
    );

    customer$.subscribe(currentCustomer => {
      if (currentCustomer) {
        this.customerForm.patchValue({
          name: currentCustomer.name,
          surname: currentCustomer.surname,
          gender: currentCustomer.gender,
          personalPhoneNumber: currentCustomer.personalPhoneNumber,
          mobileNumber: currentCustomer.mobileNumber,
          country: currentCustomer.country,
          city: currentCustomer.city,
          address: currentCustomer.address,
          id: currentCustomer.id
        });
      }
    });
  }

  updateCustomer() {
    const updatedCustomer: Customer = {
      name: this.customerForm.get('name').value,
      surname: this.customerForm.get('surname').value,
      gender: this.customerForm.get('gender').value,
      personalPhoneNumber: this.customerForm.get('personalPhoneNumber').value,
      mobileNumber: this.customerForm.get('mobileNumber').value,
      country: this.customerForm.get('country').value,
      city: this.customerForm.get('city').value,
      address: this.customerForm.get('address').value,
      id: this.customerForm.get('id').value
    };

    this.store.dispatch(new customerActions.UpdateCustomer(updatedCustomer));
  }

}
