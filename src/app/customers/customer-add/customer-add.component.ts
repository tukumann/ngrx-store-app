import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Store, State, select } from '@ngrx/store';
import * as customerActions from '../state/customer.actions';
import * as fromCustomer from '../state/customer.reducer';
import { Customer } from '../customer.model';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent implements OnInit {
  customerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromCustomer.AppState>
  ) {}

  ngOnInit() {
    this.customerForm = this.fb.group({
      id: [''],
      name: ['', Validators.compose(
        [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(/[a-zA-Z]/)]
      )],
      surname: ['', Validators.compose(
        [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(/[a-zA-Z]/)]
      )],
      gender: ['', Validators.required],
      personalPhoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(9)])],
      country: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
    });
  }

  createCustomer() {
    const newCustomer: Customer = {
      id: this.customerForm.get('id').value,
      name: this.customerForm.get('name').value,
      surname: this.customerForm.get('surname').value,
      gender: this.customerForm.get('gender').value,
      personalPhoneNumber: this.customerForm.get('personalPhoneNumber').value,
      mobileNumber: this.customerForm.get('mobileNumber').value,
      country: this.customerForm.get('country').value,
      city: this.customerForm.get('city').value,
      address: this.customerForm.get('address').value
    };
    console.log(newCustomer);

    this.store.dispatch(new customerActions.CreateCustomer(newCustomer));

    this.customerForm.reset();
  }

}
