export interface Customer {
  id?: number;
  name: string;
  surname: string;
  gender: string;
  personalPhoneNumber: string;
  mobileNumber: string;
  country: string;
  city: string;
  address: string;
}
